const fs = require('fs')
const { createCanvas, registerFont, loadImage } = require('canvas')
const { worker } = require('cluster')
var readline = require('readline');
var crypto = require('crypto');

//registerFont('SafetyMedium.otf', { family: 'Safety Medium' });

let width = 1200
let height = 1200


loadImage('./input.png').then(image => {
  let l = fs.readFileSync('./input.png')
  let hash = crypto.createHash('md5').update(l).digest("hex");
  console.log(hash) 
  height = image.height;
  width = image.width;
  let canvas = createCanvas(width, height)
  let context = canvas.getContext('2d')

  context.save();

  context.beginPath();
  context.arc(width / 2, height / 2, height / 2, 0, 2 * Math.PI, false);
  context.clip();

  context.drawImage(image, 0, 0);

  context.restore();

  let buffer = canvas.toBuffer('image/png')
  fs.writeFileSync(`./output/round_${hash}.png`, buffer)


  var rl = readline.createInterface(process.stdin, process.stdout);

  console.log("Available flags:");
  fs.readdir("./flags/", { withFileTypes: true }, (err, files) => {
    if (err)
      console.log(err);
    else {
      let i=0;
      for (f of files) {
        console.log(i + ") " + f.name);
        i++;
      }

      rl.question('Please enter the number of the flag you desire: ', (flagnr) => {
        flagfile = files[flagnr].name;
        console.log('Your flag is: ' + flagnr + " - " + flagfile);
    
    
        loadImage(`./output/round_${hash}.png`).then(image => {
          height = height + 0.1 * height;
          width = width + 0.1 * width;
    
          canvas = createCanvas(width, height)
          context = canvas.getContext('2d')
          loadImage(`./flags/${flagfile}`).then(flag => {
            context.drawImage(flag, 0, 0, width, height)
            context.drawImage(image, 0.045 * width, 0.045 * height)
            buffer = canvas.toBuffer('image/png')
            fs.writeFileSync(`./output/withflag_${hash}_${flagfile}.png`, buffer)
    
    
            context.save();
            context.fillStyle = '#000'
            context.fillRect(0, 0, width, height)
            context.beginPath();
            context.arc(width / 2, height / 2, height / 2 + 1, 0, 2 * Math.PI, false);
            context.closePath();
            context.clip();
            loadImage(`./output/withflag_${hash}_${flagfile}.png`).then(withflag => {
    
              context.drawImage(withflag, 0, 0, width, height)
              context.restore();
    
              buffer = canvas.toBuffer('image/png')
              fs.writeFileSync(`./output/out_${hash}_${flagfile}.png`, buffer)

              console.log("Okay. All files have been created in the output folder. You may close this window now.")
            });
          });
    
        });
      });
      
    }
  });
});





/*
//generate("Warning!", "This is a sign!\nThat supports\nMultiple\nLines\nin\nit...");
generate("Warning!", "Lava ahead!");
*/

function generate(title, text) {


  context.fillStyle = '#ffff02'
  context.fillRect(0, 0, width, height)

  context.font = 'bold 70pt "Safety Medium"'
  context.textAlign = 'center'
  context.textBaseline = 'top'
  context.fillStyle = '#000'


  const titleWidth = context.measureText(title).width
  // context.fillRect(600 - textWidth / 2 - 10, 0, textWidth + 20, 120)
  context.fillRect(0, 0, width, blackbannerHeight)
  context.fillStyle = '#ffff02'
  context.fillText(title, 600, 25)

  context.fillStyle = '#000'
  context.font = 'bold 50pt "Safety Medium"'
  const textHeight = context.measureText(text).emHeightDescent
  context.fillText(text, 600, blackbannerHeight + 230 - (textHeight / 2))
  fs.writeFileSync('./test.png', canvas.toBuffer('image/png'))
}
/*
*/
