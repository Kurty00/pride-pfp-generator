# Pride Profile Picture Generator
This nodejs project takes the file input.png in the project root folder and surrounds it with a pride flag from the flags directory.
It does this in multiple stages and saves all of them in the output directory.

# Usage, Requirements & Setup

Please ensure you have nodejs and a node package manager installed.

0. Clone this project or [download](https://gitlab.com/Kurty00/pride-pfp-generator/-/archive/master/pride-pfp-generator-master.zip) the current branch, if neccessary unzip it and open the project folder.
1. Put the flag you want to add to your profile picture into the `flags` folder. (or download and unzip a set of flags prepared for this [here](https://k00.eu/pfp-flags.zip))
2. Put your profile in the project folder and name it `input.png`.
3. Install the dependencies for your os using `yarn` or `npm install`.
4. Run the thing using `node .`. 
